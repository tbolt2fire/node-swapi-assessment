var axios = require("axios");
const { reset } = require("nodemon");
const url = "https://swapi.dev/api";

const people = (req, res) => {
  const sortBy = req.query.sortBy;

  fetchPeople().then((data) => {
    if (sortBy) {
      data.sort((a, b) =>
        a[sortBy] < b[sortBy] ? -1 : a[sortBy] > b[sortBy] ? 1 : 0
      );
    }
    res.send(data);
  });

  console.log("Fetching People");
};

const fetchPeople = async () => {
  let urlLink = `${url}/people`;

  const data = [];
  let page = 1;
  try {
    let response;
    do {
      response = await axios({
        method: "get",
        url: urlLink,
        params: { page },
      });
      if (response.data.results) data.push(...response.data.results);
      ++page;
    } while (response && response.data && response.data.next);
  } catch (error) {
    console.error("Error", error);
  }

  return data;
};

module.exports = { people };
