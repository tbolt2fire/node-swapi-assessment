var axios = require("axios");
const url = "https://swapi.dev/api";

const fetchUserName = async (url) => {
  let result = "";
  try {
    const response = await axios(url);
    result = response.data.name;
  } catch {
    console.error("Error");
  }
  return result;
};

const fetchPlanets = async () => {
  let urlLink = `${url}/planets`;

  const data = [];
  let page = 1;
  try {
    let response;
    do {
      response = await axios({
        method: "get",
        url: urlLink,
        params: { page },
      });
      if (response.data.results) data.push(...response.data.results);
      ++page;
    } while (response && response.data && response.data.next);
  } catch (error) {
    console.error("Error", error);
  }

  for (let i = 0; i < data.length; i++) {
    const residents = await Promise.all(
      data[i].residents.map((resident) => fetchUserName(resident))
    );
    data[i].residents = residents;
  }

  return data;
};

const planet = (req, res) => {
  fetchPlanets().then((data) => {
    res.send(data);
  });
  console.log("Fetching Planet");
};

module.exports = { planet };
