var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var { people } = require("./people");
var { planet } = require("./planet"); 

var jsonParser = bodyParser.json();

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

app.use(urlencodedParser);
app.use(jsonParser);

app.get("/", function (req, res) {
  res.send("Hello world!");
});

app.get("/people", people);
app.get("/planets", planet);



app.listen(3000);
